---
title: Hello! NexT Test Site
coauthor: Mr.J
wechat_subscriber:
  enable: true
  description: YaYa Subscribe to my blog by scanning my public wechat account.
---
# Deploy Info
{% include_code deploy.json %}
{% include_code Lastest 10 commits git-commits.txt %}

# More
{% button https://gitlab.com/JiangTJ/next-test/pipelines, CI %}

Testing Branch

{% button https://jiangtj.gitlab.io/next-test/muse, Muse %}
{% button https://jiangtj.gitlab.io/next-test/mist, Mist %}
{% button https://jiangtj.gitlab.io/next-test/pisces, Pisces %}
{% button https://jiangtj.gitlab.io/next-test/gemini, Gemini %}

Master Branch

{% button https://jiangtj.gitlab.io/next-test/master-muse, Muse %}
{% button https://jiangtj.gitlab.io/next-test/master-mist, Mist %}
{% button https://jiangtj.gitlab.io/next-test/master-pisces, Pisces %}
{% button https://jiangtj.gitlab.io/next-test/master-gemini, Gemini %}

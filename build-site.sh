#!/bin/sh

hexo clean
hexo config url https://jiangtj.gitlab.io/next-test/$2
hexo config root /next-test/$2/
hexo config theme_config.scheme $1
hexo generate
mv public temp/$2

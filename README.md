# Next Test

## Previews
- Muse <https://jiangtj.gitlab.io/next-test/muse>
- Mist <https://jiangtj.gitlab.io/next-test/mist>
- Pisces <https://jiangtj.gitlab.io/next-test/pisces>
- Gemini <https://jiangtj.gitlab.io/next-test/gemini>

## Deploy
- Environment <https://jiangtj.gitlab.io/next-test/muse/downloads/code/deploy.json>
- Pipelines: <https://gitlab.com/JiangTJ/next-test/pipelines>

## Deploy to yourself site
1. fork this project
2. modify `root` and `url` in build-site.sh
